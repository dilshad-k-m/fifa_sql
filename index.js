const { Client } = require('pg')
const fs=require('fs')
const path=require('path')
const matchespercity=require('./src/server/matchesPerCity')
const matcheswonperteam=require('./src/server/matchesWonPerTeam')
const numberofredcards=require('./src/server/NumberOfRedcards')
const toptenplayers=require('./src/server/topTenPlayers')
const client = new Client({
    host:'localhost',
    user:'postgres',
    port:5432,
    password:'dilshad@20',
    database:'postgres'
})
 
client.connect()
 
client.query(matchespercity(), (err, res) => {
    if(err){
        console.error(err);
    }else{
        fs.writeFileSync(path.join(__dirname,'/src/public/output/matchesPerCity.json'),JSON.stringify(res.rows),'utf-8')
    }
})
client.query(matcheswonperteam(),(err, res) => {
    if(err){
        console.error(err);
    }else{
        fs.writeFileSync(path.join(__dirname,'/src/public/output/matchesWonPerTeam.json'),JSON.stringify(res.rows),'utf-8')
    }
})
client.query(numberofredcards(), (err, res) => {
    if(err){
        console.error(err);
    }else{
        fs.writeFileSync(path.join(__dirname,'/src/public/output/NumberOfRedcards.json'),JSON.stringify(res.rows),'utf-8')
    }
})
client.query(toptenplayers(), (err, res) => {
    if(err){
        console.error(err);
    }else{
        fs.writeFileSync(path.join(__dirname,'/src/public/output/topTenPlayers.json'),JSON.stringify(res.rows),'utf-8')
    }
})